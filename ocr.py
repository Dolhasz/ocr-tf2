import tensorflow as tf 
import segmentation_models as sm
import numpy as np
import matplotlib.pyplot as plt


def conv_2d(channels, kernel_size):
    return tf.keras.Sequential([
        tf.keras.layers.Conv2D(channels, kernel_size, padding="same", use_bias=False),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Activation('relu')
    ])


def conv_1d(channels):
    return tf.keras.Sequential([
        tf.keras.layers.Conv1D(channels, (1,), use_bias=False),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Activation('relu')
    ])
    

def build_backbone():
    # define model
    model = sm.Unet('resnet50', encoder_weights='imagenet')
    for layer in model.layers:
        layer.trainable = False
    print(model.summary())

    new_model = tf.keras.Model(model.input, [model.get_layer('add_13').output, model.get_layer('add_15').output])

    return new_model


class OCR(tf.keras.layers.Layer):
    def __init__(self, n_classes, backbone):
        super().__init__()
        self.training = True    
        self.backbone = backbone

        self.L = tf.keras.layers.Conv2D(n_classes, (1,1))
        self.X = conv_2d(512, (3,3))

        self.phi = conv_1d(256)
        self.psi = conv_1d(256)
        self.delta = conv_1d(256)
        self.rho = conv_1d(512)
        self.g = conv_2d(512, (1,1))

        self.out = tf.keras.layers.Conv2D(n_classes, (1,1))
        self.criterion = tf.keras.losses.CategoricalCrossentropy() # FIXME: Need to ignore background class - no support for ignore_index in tf2

    def call(self, input, target):
        input_size = input.shape[1:3] # H x W
        stage_3_out, stage_4_out = self.backbone(input) # We use Resnet stage 3 output for soft mask generation and stage_4 for fine mask?


        # !!! PIXEL REPRESENTATIONS
        X = self.X(stage_4_out) # Pixel embeddings
        # !!! SOFT OBJECT REGIONS
        L = self.L(stage_3_out) # Coarse mask

        batch, height, width, n_class = L.shape
        l_flat = tf.reshape(L, (batch, n_class, -1)) # Reshape to bs x classes x pixels
        
        print('BS x classes x pixels')
        tf.print(l_flat.shape)

        M = tf.keras.activations.softmax(l_flat, -1) # flattented masks
        print('Mask: bs x classes x attention')
        tf.print(M.shape)
        
        channel = X.shape[-1]
        X_flat = tf.reshape(X, (batch, channel, -1)) # Reshape to bs x feats x pix 
        print('Embedding Features: bs x feats x pixels')
        tf.print(X_flat.shape)

        # !!! OBJECT REGION REPRESENTATIONS
        f_k = tf.linalg.matmul(M, X_flat, transpose_b=True)
        print('Correspondence between class and pixel embedding (f_k)')
        tf.print(f_k.shape)
        f_k = tf.transpose(f_k, (0,2,1))
        print('Transposed f_k')
        tf.print(f_k.shape)

        query = self.phi(f_k)
        query = tf.transpose(f_k, (0, 2, 1))
        print('Query bs x classes x key feats')
        tf.print(query.shape)

        key = self.psi(X_flat)
        print('Key shape: bs x key feats x pix')
        tf.print(key.shape)
        

        # !!! PIXEL-REGION RELATION
        logit = tf.matmul(query, key)
        print('logit = query @ key - bs x classes x pixels')
        tf.print(logit.shape)

        attn = tf.keras.activations.softmax(logit, 1)
        print('attention bs x classes x pixs')
        tf.print(attn.shape)

        delta = self.delta(tf.transpose(f_k, (0, 2, 1)))
        delta = tf.transpose(delta, (0, 2, 1))
        print('Delta bs x pixels x classes')
        tf.print(delta.shape)

        # !!! OBJECT CONTEXTURAL REPRESENTATION
        attn_sum = tf.linalg.matmul(delta, attn)
        print('Combine branches')
        tf.print(attn_sum.shape)

        X_obj = self.rho(attn_sum)
        X_obj = tf.reshape(X_obj, (batch, height, width, -1))

        concat = tf.concat([X, X_obj], 1) # concatenate image pixel embeddings with contextual features
        X_bar = self.g(concat)
        out = self.out(X_bar)
        out = tf.image.resize(out, size=input_size)

        if self.training:
            aux_out = tf.image.resize(L, size=input_size)
            loss = self.criterion(out, target)
            aux_loss = self.criterion(aux_out, target)
            return {'loss': loss, 'aux': aux_loss}, None

        else:
            return {}, out


if __name__ == "__main__":
    backbone = build_backbone()
    # some_values = backbone.predict(np.random.rand((1,512,512,3)))
    ocr = OCR(100, backbone)

    print(ocr(np.random.rand(1,512,512,3), np.random.rand(1,512,512,100)))
