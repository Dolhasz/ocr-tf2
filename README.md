# ocr-tf2

Implementation of Object-Contextual Representations for Semantic Segmentation in Tensorflow 2.

See original paper: https://arxiv.org/abs/1909.11065